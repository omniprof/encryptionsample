package com.kfwebstandard.encryptionsample;

import java.io.Serializable;
import java.util.Objects;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Bean that holds all the encryption values
 *
 * @author kfogel
 */
@Named
@SessionScoped
public class EncryptBean implements Serializable {

    private String word;

    private String encrypt;
    private String hash;
    private String salt;
    private String hashAndSalt;

    public EncryptBean() {

    }

    public EncryptBean(String word, String encrypt, String hash, String salt, String hashAndSalt) {
        this.word = word;
        this.encrypt = encrypt;
        this.hash = hash;
        this.salt = salt;
        this.hashAndSalt = hashAndSalt;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getHashAndSalt() {
        return hashAndSalt;
    }

    public void setHashAndSalt(String hashAndSalt) {
        this.hashAndSalt = hashAndSalt;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.word);
        hash = 53 * hash + Objects.hashCode(this.encrypt);
        hash = 53 * hash + Objects.hashCode(this.hash);
        hash = 53 * hash + Objects.hashCode(this.salt);
        hash = 53 * hash + Objects.hashCode(this.hashAndSalt);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EncryptBean other = (EncryptBean) obj;
        if (!Objects.equals(this.word, other.word)) {
            return false;
        }
        if (!Objects.equals(this.encrypt, other.encrypt)) {
            return false;
        }
        if (!Objects.equals(this.hash, other.hash)) {
            return false;
        }
        if (!Objects.equals(this.salt, other.salt)) {
            return false;
        }
        if (!Objects.equals(this.hashAndSalt, other.hashAndSalt)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EncryptBean{" + "word=" + word + ", encrypt=" + encrypt + ", hash=" + hash + ", salt=" + salt + ", hashAndSalt=" + hashAndSalt + '}';
    }

}
