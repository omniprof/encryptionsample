package com.kfwebstandard.encryptionsample.password;

import com.kfwebstandard.encryptionsample.EncryptBean;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Base64;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * This class contains methods that hashes and salts passwords
 * 
 * @author kfogel
 */
@Named
@SessionScoped
public class PasswordUtil implements Serializable{    
    
    @Inject
    private EncryptBean encryptBean;
    
    public String doPassword() throws NoSuchAlgorithmException {
        encryptBean.setEncrypt(hashPassword(encryptBean.getWord()));
        encryptBean.setSalt(getSalt());
        encryptBean.setHashAndSalt(hashAndSaltPassword(encryptBean.getWord()));
        return null;
    }

    public String hashPassword(String password)
            throws NoSuchAlgorithmException {        
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        md.update(password.getBytes());
        byte[] mdArray = md.digest();
        StringBuilder sb = new StringBuilder(mdArray.length * 2);
        for (byte b : mdArray) {
            int v = b & 0xff;
            if (v < 16) {
                sb.append('0');
            }
         sb.append(Integer.toHexString(v));
        }        
        return sb.toString();        
    }
    
    public String getSalt() {
        Random r = new SecureRandom();
        byte[] saltBytes = new byte[32];
        r.nextBytes(saltBytes);
        return Base64.getEncoder().encodeToString(saltBytes);
    }
    
    public String hashAndSaltPassword(String password)
            throws NoSuchAlgorithmException {
        String salt = getSalt();
        return hashPassword(password + salt);
    }
}